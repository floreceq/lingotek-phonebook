var http = require('http'),
	express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser');

var environment = process.env.NODE_ENV;
//var environment = 'build';
var app = express();

// Configuration - environments
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

switch (environment) {
    case 'build':
        console.log('** BUILD **');
        var port = process.env.PORT || 7000;
        var cons = require('consolidate'),
        routes = require('./build/routes'),
        api = require('./build/routes/api');    

        app.engine('html', cons.swig);
        app.set('views', path.join(__dirname, '/build/views'));
        app.set('view engine', 'html');
        app.use(express.static(path.join(__dirname, '/build/public')));         
        break;
    default:
        console.log('** DEV **');   
        var port = process.env.PORT || 3000; 
        var routes = require('./routes'),
        api = require('./routes/api'); 

        app.use(express.static(path.join(__dirname, '/public')));
		app.set('view engine', 'jade');
		app.set('views', path.join(__dirname, '/views'));    
        app.use(require('stylus').middleware(__dirname + '/public'));                         		
        break;
}

// Routes

app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

// JSON API

app.get('/api/lists', api.lists);
app.post('/api/contact', api.create);
app.get('/api/contact/:id', api.read);
app.put('/api/contact/:id', api.update);
app.delete('/api/contact/:id', api.delete);

// redirect all others to the index
app.get('*', routes.index);

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
    console.log('env = ' + app.get('env') +
                '\n__dirname = ' + __dirname +
                '\nprocess.cwd = ' + process.cwd());
});