var PhonebookApp = angular.module('PhonebookApp', ["ui.router", "ui.bootstrap"])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider){
      
      $locationProvider.html5Mode(true); 
      
      // For any unmatched url, send to /list
      $urlRouterProvider.otherwise("/");

      $stateProvider
      .state('/', {
        url: "/",
        templateUrl: 'partials/index',
        controller: 'IndexCtrl'
      });
    }); 

    PhonebookApp.factory('PbModalEvent', function($modalStack) {
        return {
            close: function() {
                $modalStack.dismissAll();
            }
        };
    });

    PhonebookApp.run(function($rootScope, PbModalEvent) {
      $rootScope.closeModule = PbModalEvent;
    });    

    PhonebookApp.directive('phoneInput', function($filter, $browser) {
      return {
          require: 'ngModel',
          link: function($scope, $element, $attrs, ngModelCtrl) {
              var listener = function() {
                  //var value = $element.val().replace(/[^0-9]/g, '');
                  var value = $element.val();
                  $element.val($filter('tel')(value, false));
              };
              // This runs when we update the text field
              ngModelCtrl.$parsers.push(function(viewValue) {
                  //return viewValue.replace(/[^0-9]/g, '').slice(0,12);
                  return viewValue;
              });
              // This runs when the model gets updated on the scope directly and keeps our view in sync
              ngModelCtrl.$render = function() {
                  $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
              };
              $element.bind('change', listener);
              $element.bind('keydown', function(event) {
                  var key = event.keyCode;
                  // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                  // This lets us support copy and paste too
                  if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                      return;
                  }
                  $browser.defer(listener); // Have to do this or changes don't get picked up properly
              });
              $element.bind('paste cut', function() {
                  $browser.defer(listener);
              });
          }
      };
    });

    PhonebookApp.filter('tel', function () {
      return function (tel) {            
        if (!tel) { return ''; }
        var value = tel.toString().trim();        
        if (value.match(/[^0-9]/)) {
          return tel;            
        }        
        var number = 0;       
        if(value){
          if(value.length>4){
            number = value.slice(0, 4) + '-' + value.slice(4,12);
          }
          else{
            number = number;
          }
          return number.trim();
        }
      };
    });    