var gulp    = require('gulp'),
	jade = require('gulp-jade'),
  stylus = require('gulp-stylus'),
	inject = require('gulp-inject'),
	path = require('path');

path.join(__dirname, '/public');
 
var paths = {
  route: './routes/*.js',
  public: './public/**/*',
  jade: './views/**/*.jade'
}

gulp.task('default', ['templates','routes','copy']);

gulp.task('templates', function() {
	var YOUR_LOCALS = {}; 
	return gulp.src(paths.jade)
	.pipe(jade({
	  locals: YOUR_LOCALS
	}))
	.pipe(gulp.dest('./build/views'))
});

gulp.task('routes', function() {
   return gulp.src(paths.route)
   .pipe(gulp.dest('./build/routes'));
});

gulp.task('copy', function () {
    return gulp.src(paths.public)
    .pipe(gulp.dest('./build/public'));
});

// gulp.task('scripts', ['templates'], function () {

// });