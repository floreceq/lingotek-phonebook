PhonebookApp.controller('IndexCtrl', function ($scope, $http, $modal) {
	$http.get('/api/lists').
	success(function (data, status, headers, config) {
	  $scope.contacts = data.contacts;  
	}); 
	$scope.openCreateModal = function () {	
	    var modalInstance =  $modal.open({
		  		templateUrl: 'partials/create'
	    	});    
	};		
	$scope.contact = $scope.form = {};
	$scope.openViewModal = function (id, modalForm) {	
	    var modalInstance =  $modal.open({
		  		templateUrl: 'partials/' + modalForm,
		  		controller: function ($scope, $http){
		        $http.get('/api/contact/' + id).
		          success(function (data) {
		          	$scope.form = data.contact;
		            $scope.contact = data.contact;
		            $scope.form['id'] = $scope.contact['id'] = id;
		          });
		        } 
	    	});    
	};		
});

PhonebookApp.controller('CreateCtrl', function ($scope, $http, $modalStack, $state) {
	$scope.form = {};	
	$scope.createContact = function () {
		$http.post('/api/contact', $scope.form).
		success(function (data) {
		   $state.go($state.current, {}, {reload: true});
		   $modalStack.dismissAll();
		});        
	};
});

PhonebookApp.controller('DeleteCtrl', function ($scope, $http, $modalStack, $state) {	
	$scope.deleteContact = function (id) {
		$http.delete('/api/contact/' + id).
		success(function (data) {
		   $state.go($state.current, {}, {reload: true});
		   $modalStack.dismissAll();
		});       
	};
});

PhonebookApp.controller('EditCtrl', function ($scope, $http, $modalStack, $state) {
	$scope.editContact = function (id, name, phone) {
		$scope.form = { name: name, phone: phone };
		$http.put('/api/contact/' + id, $scope.form).
		success(function (data) {
		   $state.go($state.current, {}, {reload: true});
		   $modalStack.dismissAll();
		});      
	};
});