/*
 * Serve JSON to our AngularJS client
 */
//"data" acts like an in-memory "database"
var data = {
  "contacts": [
    { name:'A1', phone:'0111-9876543' },{ name:'A2', phone:'0222-9876543' },{ name:'A3', phone:'0333-9876543' },
    { name:'B1', phone:'0444-9876543' }
  ]
};

// GET

exports.lists = function (req, res) {
  var contacts = [];
  data.contacts.forEach(function (post, i) {
    contacts.push({
      id: i,
      name: post.name,
      phone: post.phone
    });
  });
  res.json({
    contacts: contacts
  });
};

// POST 

exports.create = function (req, res) { 
  data.contacts.push(req.body);
  res.json(req.body);
};

// GET
exports.read = function (req, res) {
  var id = req.params.id;
  if (id >= 0 && id < data.contacts.length) {
    res.json({
      contact: data.contacts[id]
    });    
  } else {
    res.json(false);
  }
};


// PUT

exports.update = function (req, res) {
  var id = req.params.id;
  if (id >= 0 && id < data.contacts.length) {
    data.contacts[id] = req.body;
    res.json(true);
  } else {
    res.json(false);
  }
};

// DELETE

exports.delete = function (req, res) {
  var id = req.params.id;

  if (id >= 0 && id < data.contacts.length) {
    data.contacts.splice(id, 1);
    res.json(true);
  } else {
    res.json(false);
  }
};